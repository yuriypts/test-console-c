﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace MultithreadedConsoleApplication
{
    class Program
    {
        static bool exit = false;
        static volatile CommonContainer container;
        static AutoResetEvent ready;
        static Random rnd;

        static void Main(string[] args)
        {
            int X = 0;
            int Y = 1;

            Console.WriteLine("Please enter 'X' int value from 1 to 64 (including)");
            var xValue = Console.ReadLine();

            Console.WriteLine(new string('-', 50));

            Console.WriteLine("Please enter 'Y' count items in common container");
            var yValue = Console.ReadLine();

            Console.WriteLine(new string('-', 50));

            try
            {
                X = int.Parse(xValue);
                Y = int.Parse(yValue);

                if (X < 1 || Y < 1 || X > 61)
                    throw new Exception();
            }
            catch (Exception err)
            {
                Console.WriteLine("'X' and 'Y' should be integer and bigger than 1", err.Message);
                Console.ReadLine();
                return;
            }

            Thread[] threads = new Thread[X];
            container = new CommonContainer(Y);
            ready = new AutoResetEvent(true);
            rnd = new Random();

            Console.WriteLine("Threads: {0}, threshold common container: {1}", X, Y);

            for (int i = 0; i < threads.Length; i++)
            {
                threads[i] = new Thread(Add);
                threads[i].IsBackground = true;
                threads[i].Start(i);
                Thread.Sleep(100);
            }

            Console.WriteLine("Enter to abort");
            Console.ReadLine();

            exit = true;
            for (int i = 0; i < threads.Length; i++)
            {
                threads[i].Join();
                Console.WriteLine("Thread {0}: {1} items", i, container.GetItemCount(i));
            }

            Console.ReadLine();
        }

        private static void Add(object i)
        {
            bool parse = Int32.TryParse(i.ToString(), out var item);

            if (parse)
            {
                for (; ; )
                {
                    try
                    {
                        if (exit)
                            break;

                        ready.WaitOne();

                        container.AddItem(item);

                        ready.Set();

                        Thread.Sleep(rnd.Next(0, 1000));
                    }
                    catch (Exception err)
                    {
                        Console.WriteLine("Error {0}: {1}", item, err.Message);
                        return;
                    }
                }
            }
        }
    }
}
