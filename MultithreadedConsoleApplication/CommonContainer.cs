﻿using System;
using System.Collections.Generic;

namespace MultithreadedConsoleApplication
{
    public class CommonContainer
    {
        private List<int> container;
        private int capacity;
        private int maximum;

        public CommonContainer(int capacity)
        {
            container = new List<int>();
            maximum = 0;
            this.capacity = capacity;
        }

        public int Maximum
        {
            get { return maximum; }
        }

        public int GetItemCount(int item)
        {
            int count = 0;
            for (int i = 0; i < container.Count; i++)
            {
                if (container[i] == item)
                    count++;
            }
            return count;
        }

        public void AddItem(int item)
        {
            if (container.Count > 0)
            {
                if ((container[container.Count - 1] != item) ||
                    (container.Count >= capacity))
                    DeleteLast();
            }
            container.Add(item);
            if (container.Count > maximum)
                maximum = container.Count;

            Console.Title = container.Count.ToString() + "/" + capacity.ToString();
        }

        private void DeleteLast()
        {
            container.RemoveAt(container.Count - 1);
        }
    }
}
